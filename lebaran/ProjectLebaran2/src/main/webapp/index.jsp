<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<title>Product Insert Page</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png" href="images/icons/favicon.ico" />
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/noui/nouislider.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="css/util.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>


	<div class="container-contact100">
		<div class="wrap-contact100">
			<form method="post" action="addJSON"
				class="contact100-form validate-form">
				<span class="contact100-form-title"> Product Insert Page</span>
				<div class="wrap-input100 input100-select bg1">
					<span class="label-input100">Article Number</span> <input
						name="articleNumber" class="input100" type="text"
						placeholder="Enter Article Number">
				</div>
				<div class="wrap-input100 input100-select bg1">
					<span class="label-input100">Name</span> <input name="name"
						class="input100" type="text" placeholder="Enter Name">
				</div>

				<div class="wrap-input100 validate-input bg0 rs1-alert-validate">
					<span class="label-input100">Description</span>
					<textarea id="description" class="input100" name="description"
						placeholder="Your message here..."></textarea>
				</div>
				<div class="wrap-input100 validate-input bg1">
					<span class="label-input100">Image Url</span> <input
						class="input100" type="text" name="imageUrl"
						placeholder="Enter Your Name">
				</div>
				<div class="wrap-input100 input100-select bg1">
					<span class="label-input100">Stock</span> <input name="stock"
						class="input100" type="text" placeholder="Enter Product Stock">
				</div>



				<div class="wrap-input100 input100-select bg1">
					<span class="label-input100">Brand</span>
					<div>

						<select class="js-select2" name="brand">
							<c:forEach items="${result}" var="brand" varStatus="loop">
								<option value="${brand.id}">${brand.name}</option>
							</c:forEach>
						</select>

						<div class="dropDownSelect2"></div>
					</div>
				</div>
				<div class="container-contact100-form-btn">
					<button type="submit" value="submit" class="contact100-form-btn">
						<span> Submit <i class="fa fa-long-arrow-right m-l-7"
							aria-hidden="true"></i>
						</span>
					</button>
				</div>


			</form>
			<br>
			<button onclick="location.href='http://localhost:8080/listProduct'"
				class="contact100-form-btn">
				<span>List Product<i class="fa fa-long-arrow-right m-l-7"
					aria-hidden="true"></i>
				</span>
			</button>
		</div>
	</div>



	<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<script>
		$(".js-select2").each(function() {
			$(this).select2({
				minimumResultsForSearch : 20,
				dropdownParent : $(this).next('.dropDownSelect2')
			});

			$(".js-select2").each(function() {
				$(this).on('select2:close', function(e) {
					if ($(this).val() == "Please chooses") {
						$('.js-show-service').slideUp();
					} else {
						$('.js-show-service').slideUp();
						$('.js-show-service').slideDown();
					}
				});
			});
		})
	</script>
	<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/noui/nouislider.min.js"></script>
	<script>
		var filterBar = document.getElementById('filter-bar');

		noUiSlider.create(filterBar, {
			start : [ 1500, 3900 ],
			connect : true,
			range : {
				'min' : 1500,
				'max' : 7500
			}
		});

		var skipValues = [ document.getElementById('value-lower'),
				document.getElementById('value-upper') ];

		filterBar.noUiSlider.on('update', function(values, handle) {
			skipValues[handle].innerHTML = Math.round(values[handle]);
			$('.contact100-form-range-value input[name="from-value"]').val(
					$('#value-lower').html());
			$('.contact100-form-range-value input[name="to-value"]').val(
					$('#value-upper').html());
		});
	</script>
	<!--===============================================================================================-->
	<script src="js/main.js"></script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async
		src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
	<script>
		function make_json(form) {

			var json = {
				"articleNumber" : form.articleNumber.value,
				"name" : form.namaBarang.value,
				"description" : form.description.value,
				"imageUrl" : form.imageUrl.value,
				"stock" : form.stock.value,
				"brand" : Number(form.brand.value)
			};
			var html = JSON.stringify(json, 0, 100);
			var Url = "http://localhost:8080/addJSON";
			var xhr = new XMLHttpRequest();
			xhr.open('POST', Url, false);
			xhr.setRequestHeader("Content-type", "application/json");
			xhr.send(html);
		};
	</script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-23581568-13');
	</script>

</body>
</html>