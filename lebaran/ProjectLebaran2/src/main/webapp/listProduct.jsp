<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<style>
#tableProduct {
	font-family: Arial, Helvetica, sans-serif;
	border-collapse: collapse;
	width: 100%;
}

#tableProduct td, #tableProduct th {
	border: 1px solid #ddd;
	padding: 8px;
}

#tableProduct tr:nth-child(even) {
	background-color: #f2f2f2;
}

#tableProduct tr:hover {
	background-color: #ddd;
}

#tableProduct th {
	padding-top: 12px;
	padding-bottom: 12px;
	text-align: left;
	background-color: #4CAF50;
	color: white;
}
</style>
<title>List Product</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png" href="images/icons/favicon.ico" />
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/noui/nouislider.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="css/util.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>


	<div class="container-contact100">
		<div class="wrap-contact100">
			<span class="contact100-form-title"> List Product</span>
			<table id="tableProduct">

				<tr>
					<th>IMAGE URL</th>
					<th>ARTICLE NUMBER</th>
					<th>BRAND</th>
					<th>NAME</th>
					<th>DESCRIPTION</th>
					<th>STOCK</th>
					<th>ACTION</th>
				</tr>
				<c:forEach items="${result}" var="list">
					<tr>
						<td><img SRC="${list.imageUrl}" width="100" height="100"></td>
						<td>${list.articleNumber}</td>
						<td>${list.brand.getName()}</td>
						<td>${list.name}</td>
						<td>${list.description}</td>
						<td>${list.stock}</td>
						<td>
							<button onclick="deleteFunction('${list.getId()}')">Delete</button>
							<button onclick="location.href='http://localhost:8080/listProduct/details/${list.getId()}'">Details</button>
						</td>
					</tr>
				</c:forEach>
			</table>
			<br>
			<button onclick="location.href='http://localhost:8080'" class="contact100-form-btn">
				<span><i class="fa fa-long-arrow-left m-l-7"
					aria-hidden="true"></i>Product Insert Page
				</span>
			</button>
		</div>
	</div>

	<script>
		function deleteFunction(argument) {
			var txt;
			var r = confirm("Apakah anda yakin akan menghapus data? ");
			if (r == true) {
				location.href = "http://localhost:8080/listProduct/delete/"
						+ argument;
			} else {
				txt = "You pressed Cancel!";
			}
		}
		
	</script>

	<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<script>
		$(".js-select2").each(function() {
			$(this).select2({
				minimumResultsForSearch : 20,
				dropdownParent : $(this).next('.dropDownSelect2')
			});

			$(".js-select2").each(function() {
				$(this).on('select2:close', function(e) {
					if ($(this).val() == "Please chooses") {
						$('.js-show-service').slideUp();
					} else {
						$('.js-show-service').slideUp();
						$('.js-show-service').slideDown();
					}
				});
			});
		})
	</script>
	<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/noui/nouislider.min.js"></script>
	<script>
		var filterBar = document.getElementById('filter-bar');

		noUiSlider.create(filterBar, {
			start : [ 1500, 3900 ],
			connect : true,
			range : {
				'min' : 1500,
				'max' : 7500
			}
		});

		var skipValues = [ document.getElementById('value-lower'),
				document.getElementById('value-upper') ];

		filterBar.noUiSlider.on('update', function(values, handle) {
			skipValues[handle].innerHTML = Math.round(values[handle]);
			$('.contact100-form-range-value input[name="from-value"]').val(
					$('#value-lower').html());
			$('.contact100-form-range-value input[name="to-value"]').val(
					$('#value-upper').html());
		});
	</script>
	<!--===============================================================================================-->
	<script src="js/main.js"></script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async
		src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>

	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-23581568-13');
	</script>

</body>
</html>