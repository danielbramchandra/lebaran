package id.co.nexsoft.ProjectLebaran2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectLebaran2Application {

	public static void main(String[] args) {
		SpringApplication.run(ProjectLebaran2Application.class, args);
	}

}
