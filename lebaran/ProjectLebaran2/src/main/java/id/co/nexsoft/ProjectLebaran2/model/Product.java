package id.co.nexsoft.ProjectLebaran2.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	String name;
	String articleNumber;
	String description;
	String imageUrl;
	int stock;
	@ManyToOne
	Brand brand;

	public Product() {
	}



	public Product(String name, String articleNumber, String description, String imageUrl, int stock, Brand brand) {
		this.name = name;
		this.articleNumber = articleNumber;
		this.description = description;
		this.imageUrl = imageUrl;
		this.stock = stock;
		this.brand = brand;
	}

	public String getName() {
		return name;
	}
	public int getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getArticleNumber() {
		return articleNumber;
	}

	public void setArticleNumber(String articleNumber) {
		this.articleNumber = articleNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

}
