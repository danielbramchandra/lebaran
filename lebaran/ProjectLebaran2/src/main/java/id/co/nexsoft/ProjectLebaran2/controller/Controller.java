package id.co.nexsoft.ProjectLebaran2.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import id.co.nexsoft.ProjectLebaran2.BrandRepository;
import id.co.nexsoft.ProjectLebaran2.ProductRepository;
import id.co.nexsoft.ProjectLebaran2.model.Brand;
import id.co.nexsoft.ProjectLebaran2.model.Product;

@RestController
public class Controller {
	@Autowired
	private BrandRepository brandRepo;
	@Autowired
	private ProductRepository productRepo;

	@GetMapping("/")
	public ModelAndView welcomePage() {
		List<Brand>brand = getAllBrand();
		ModelAndView mv = new ModelAndView("index");
		mv.addObject("result", brand);
		return mv;
	}

	@GetMapping("/listProduct")
	public ModelAndView showview() {
		List<Product> listProduct = getAllProduct();
		ModelAndView mv = new ModelAndView("listProduct");
		mv.addObject("result", listProduct);
		return mv;
	}

	@GetMapping("/listProduct/details/{id}")
	public ModelAndView detailsProduct(@PathVariable(value = "id") int id) {
		Product product = getProductById(id);
		Brand brand  = product.getBrand();
		ModelAndView mv = new ModelAndView("details");	
		mv.addObject("product", product);		
		return mv;
	}
	@RequestMapping(value = "/listProduct/delete/{id}", method = RequestMethod.GET)
	public ModelAndView deleteProduct(@PathVariable(value = "id") int id) {
		productRepo.deleteById(id);
		ModelAndView mv = new ModelAndView();
		mv.setViewName("listProduct");
		return mv;
	}

	@PostMapping("/addJSON")
	public ModelAndView add(Product product) {
		productRepo.save(product);
		return new ModelAndView("index");
	}

	@GetMapping("/brand")
	public List<Brand> getAllBrand() {
		return brandRepo.findAll();
	}

	@GetMapping("/product")
	public List<Product> getAllProduct() {
		return productRepo.findAll();
	}


	@GetMapping("/product/{id}")
	public Product getProductById(@PathVariable(value = "id") int id) {
		return productRepo.findById(id);
	}

}
