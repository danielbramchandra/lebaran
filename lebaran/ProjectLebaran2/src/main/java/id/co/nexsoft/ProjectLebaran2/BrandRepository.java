package id.co.nexsoft.ProjectLebaran2;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.ProjectLebaran2.model.Brand;



public interface BrandRepository extends CrudRepository<Brand, Integer>{
	Brand findById(int id);
	List<Brand> findAll();
	void deleteById(int id);
}
