package id.co.nexsoft.ProjectLebaran2;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.ProjectLebaran2.model.Product;

//import id.co.nexsoft.Formative15.model.Country;

public interface ProductRepository extends CrudRepository<Product, Integer>{
	Product findById(int id);
	List<Product> findAll();
	void deleteById(int id);
	Product save(Product product);
}
