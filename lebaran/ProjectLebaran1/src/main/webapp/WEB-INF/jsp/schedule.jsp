<%@page import="java.util.Comparator"%>
<%@page import="java.util.Collections"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.time.LocalDate"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<style>
* {
	box-sizing: border-box;
}

ul {
	list-style-type: none;
}

body {
	font-family: Verdana, sans-serif;
}

.month {
	padding: 70px 25px;
	width: 100%;
	background: #1abc9c;
	text-align: center;
}

.month ul {
	margin: 0;
	padding: 0;
}

.month ul li {
	color: white;
	font-size: 20px;
	text-transform: uppercase;
	letter-spacing: 3px;
}

.month .prev {
	float: left;
	padding-top: 10px;
}

.month .next {
	float: right;
	padding-top: 10px;
}

.weekdays {
	margin: 0;
	padding: 10px 0;
	background-color: #ddd;
}

.weekdays li {
	display: inline-block;
	width: 13.6%;
	color: #666;
	text-align: center;
}

.days {
	padding: 10px 0;
	background: #eee;
	margin: 0;
}

.days li {
	list-style-type: none;
	display: inline-block;
	width: 13.6%;
	text-align: center;
	margin-bottom: 50px;
	font-size: 20px;
	color: #777;
}

.days li .active {
	padding: 5px;
	background: #1abc9c;
	color: white !important
}

/* Add media queries for smaller screens */
@media screen and (max-width:720px) {
	.weekdays li, .days li {
		width: 13.1%;
	}
}

@media screen and (max-width: 420px) {
	.weekdays li, .days li {
		width: 12.5%;
	}
	.days li .active {
		padding: 2px;
	}
}

@media screen and (max-width: 290px) {
	.weekdays li, .days li {
		width: 12.2%;
	}
}
</style>
</head>
<body>
	<div class="month">
		<ul>
			<c:if test=""></c:if>
			<li style="cursor: pointer;"
				onclick="location.href='http://localhost:8080/${id-1}'" class="prev">&#10094;</li>
			<li style="cursor: pointer;"
				onclick="location.href='http://localhost:8080/${id+1}'" class="next">&#10095;</li>
			<li>${month}<br> <span style="font-size: 18px">2021</span>
			</li>
		</ul>
	</div>
	<ul class="weekdays">
		<li>Monday</li>
		<li>Tuesday</li>
		<li>Wednesday</li>
		<li>Thursday</li>
		<li>Friday</li>
		<li>Saturday</li>
		<li>Sunday</li>
	</ul>

	<ul class="days">

		
		<c:forEach items="${date}" var="entry">
			<li style="cursor: pointer;"
				onclick="myFunction('${entry.key}','${entry.value}')">${entry.key.getDayOfMonth()}</li>
		</c:forEach>
	</ul>

</body>
<script>
	function myFunction(tanggal, harga) {
		alert("Harga Tiket Untuk Tanggal " + tanggal + " Sebesar $" + harga);
	}
</script>
</html>
