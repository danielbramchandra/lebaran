package id.co.nexsoft.ProjectLebaran1;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ClassController {
	@GetMapping("/")
	public ModelAndView showview() {
		TiketPesawat tiket = new TiketPesawat();
		ModelAndView mv = new ModelAndView("schedule");
		return mv;
	}

	@GetMapping("/{id}")
	public ModelAndView ticketSchedule(@PathVariable(value = "id") int id) {
		TiketPesawat tiket = new TiketPesawat();
		ModelAndView mv = new ModelAndView("schedule");
		List<String> day = List.of("MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY");
		mv.addObject("day", day);
		mv.addObject("id", id);
		mv.addObject("date", tiket.listHargaPerMonth.get(id));
		mv.addObject("month", tiket.getMonth().get(id - 1));
		return mv;
	}
}
