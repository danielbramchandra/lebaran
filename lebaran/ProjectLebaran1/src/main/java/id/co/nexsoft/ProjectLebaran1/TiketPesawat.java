package id.co.nexsoft.ProjectLebaran1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TiketPesawat {
	LocalDate pemesananTiket;
	LocalDate keberangkatan;
	LocalDate startSummer = LocalDate.of(2021, 6, 28);
	LocalDate startWinter = LocalDate.of(2021, 12, 21);
	LocalDate endSummer = LocalDate.of(2021, 9, 22);
	LocalDate endWinter = LocalDate.of(2022, 3, 20);
	double diskon15Persen = 0.15;
	double diskon025Persen = 0.025;
	double tambahan10persen = 0.1;
	double tambahan20persen = 0.2;
	double tambahanSummer = 0.2;
	double tambahanWinter = 0.15;
	static final double HARGA_TIKET = 600;
	Map<Integer, Map<LocalDate, Double>> listHargaPerMonth = new LinkedHashMap<Integer, Map<LocalDate, Double>>();
	List<String> month = new ArrayList<String>();

	public TiketPesawat() {
		addList();

	}

	public List<String> getMonth() {
		return month;
	}

	public void addList() {
//		keberangkatan = pemesananTiket;
		int j = 1;
		for (int i = 1; i <= 365; i++) {
			pemesananTiket = LocalDate.now();
			Map<LocalDate, Double> temp = null;
			LocalDate berangkat = pemesananTiket.plusDays(i - 1);
			double tempHarga = HARGA_TIKET;
			if (berangkat.equals(pemesananTiket.plusDays(1)) || berangkat.equals(pemesananTiket)) {
				tempHarga = tempHarga + (HARGA_TIKET * tambahan20persen);
			}
			if (berangkat.isAfter(pemesananTiket.plusDays(2)) && berangkat.isBefore(pemesananTiket.plusDays(5))) {
				tempHarga = tempHarga + (HARGA_TIKET * tambahan10persen);
			}
			if (berangkat.isAfter(pemesananTiket.plusMonths(6)) && berangkat.isBefore(pemesananTiket.plusMonths(9))) {
				tempHarga = tempHarga - (HARGA_TIKET * diskon025Persen);
			}
			if (berangkat.isAfter(pemesananTiket.plusMonths(10)) && berangkat.isBefore(pemesananTiket.plusMonths(12))) {
				tempHarga = tempHarga - (HARGA_TIKET * diskon15Persen);
			}
			if (berangkat.isAfter(startSummer) && berangkat.isBefore(endSummer)) {
				tempHarga = tempHarga + (HARGA_TIKET * tambahanSummer);
			}
			if (berangkat.isAfter(startWinter) && berangkat.isBefore(endWinter)) {
				tempHarga = tempHarga + (HARGA_TIKET * tambahanWinter);
			}
			if (!listHargaPerMonth.containsKey(j)) {
				temp = new LinkedHashMap<LocalDate, Double>();
			} else {
				temp = listHargaPerMonth.get(j);
			}
			temp.put(berangkat, tempHarga);
			listHargaPerMonth.put(j, temp);
			if (berangkat.plusDays(1).getMonthValue() != (berangkat.getMonthValue())) {
				month.add(berangkat.getMonth().toString());
				j++;
			}

		}

	}
}
